<?php

// Establish a connection to the database.
$dbConnection = pg_connect("host=ddev-php-application-db dbname=mystore user=db password=db") or die('DB connection failed: ' . pg_last_error());

// Execute initial queries to fill the table with data.
$dropTableQuery = "DROP TABLE IF EXISTS articles;";
$dropTableResult = pg_query($dbConnection, $dropTableQuery) or die('Table deletion query failed: ' . pg_last_error());

$createTableQuery = "CREATE TABLE articles (
  id serial NOT NULL,
  name varchar(255) NOT NULL,
  price numeric(12, 2) NOT NULL,
  description text NOT NULL,
  PRIMARY KEY (id)
);";
$createTableResult = pg_query($dbConnection, $createTableQuery) or die('Table creation query failed: ' . pg_last_error());

$insertQuery1 = "INSERT INTO articles (name, price, description) VALUES (
  'Vase',
  '5.6',
  'A beautiful vase'
), (
  'Shoes',
  '65.99',
  'A pair of black sports shoes'
);";
$insertResult1 = pg_query($dbConnection, $insertQuery1) or die('Insert query failed: ' . pg_last_error());

// Execute a query to get the data from the table.
$selectQuery = "SELECT * FROM articles;";
$selectResult = pg_query($dbConnection, $selectQuery) or die('Select query failed: ' . pg_last_error());

// Display the data in an HTML table.
echo "<table>";
echo "<tr><th>ID</th><th>Name</th><th>Price</th><th>Description</th></tr>";
while ($row = pg_fetch_row($selectResult)) {
    echo "<tr>" . join("", array_map(fn($value) => "<td>$value</td>", $row)) . "</tr>";
}
echo "</table>";

// Close the database connection.
pg_close($dbConnection);
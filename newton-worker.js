importScripts("polynomial-function.js");

/**
 * Web Worker for Newton's method.
 * Try to find the root `x1` of the polynomial function `f` starting from `x0`.
 * If the root is found, send it back to the main thread.
 * Otherwise, if `x1` exceeds the lower or upper bound, no root is passed with the message.
 */
self.addEventListener("message", (e) => {
  let { x0 } = e.data;
  const { f, lowerBound, upperBound, precision } = e.data;
  Object.setPrototypeOf(f, PolynomialFunction.prototype);

  const epsilon = Math.pow(10, -(precision + 1));
  const fPrime = f.derivative();

  while (true) {
    const x1 = x0 - f.evaluate(x0) / fPrime.evaluate(x0);
    if (x1 < lowerBound || x1 > upperBound) {
      self.postMessage({ success: false });
      break;
    }
    if (Math.abs(x1 - x0) < epsilon) {
      self.postMessage({
        success: true,
        x1: parseFloat(x1.toFixed(precision)),
      });
      break;
    }
    x0 = x1;
  }
});

/**
 * Represent a polynomial function.
 *
 * @see https://en.wikipedia.org/wiki/Polynomial#Polynomial_functions
 */
class PolynomialFunction {
  /**
   * Create a new PolynomialFunction from an array of coefficients.
   * The array is ordered from the highest to the lowest degree, the lowest exponent is 0.
   *
   * @param {number[]} coefficients The coefficients of the polynomial function.
   * @returns {PolynomialFunction} The new PolynomialFunction.
   * @example new PolynomialFunction([2, 3, 4]) // 2x^2 + 3x + 4
   */
  constructor(coefficients) {
    this.coefficients = coefficients;
  }

  /**
   * Evaluate the polynomial function for a given value.
   *
   * @param {number} x The value to evaluate the polynomial function for.
   * @returns {number} The result of the evaluation.
   */
  evaluate(x) {
    let result = 0;
    for (const [coefficient, exponent] of this.coefficientsWithExponents()) {
      result += coefficient * Math.pow(x, exponent);
    }
    return result;
  }

  /**
   * Build the derivative of the polynomial function.
   *
   * @returns {PolynomialFunction} A new PolynomialFunction representing the derivative of the polynomial function.
   */
  derivative() {
    const newCoefficients = [];
    for (const [coefficient, exponent] of this.coefficientsWithExponents()) {
      if (exponent > 0) newCoefficients.push(exponent * coefficient);
    }
    return new PolynomialFunction(newCoefficients);
  }

  /**
   * Return a string representation of the polynomial function.
   *
   * @returns {string} The string representation of the polynomial function.
   */
  toString() {
    let result = "";
    for (const [
      coefficient,
      exponent,
      index,
    ] of this.coefficientsWithExponents()) {
      // Skip coefficients of zero.
      if (coefficient === 0) continue;

      // If it is not the first coefficient, add a plus or minus sign in front of it.
      if (result.length > 0) result += coefficient > 0 ? " + " : " - ";

      // Add the coefficient only for `x^0` if it is not 1 or -1.
      if (exponent === 0 || Math.abs(coefficient) !== 1)
        result += Math.abs(coefficient);

      // Add `x` if the exponent is not zero.
      if (exponent !== 0) result += "x";

      // Add the exponent if it is larger than 1 or smaller than -1.
      if (Math.abs(exponent) > 1) result += "^" + exponent;
    }
    return result;
  }

  /**
   * Generate the respective exponents for the coefficients of the polynomial function.
   * The coefficient, the exponent and the index of the coefficient are provided in an array.
   *
   * @generator
   * @yields {[number, number, number]} The coefficient, the exponent and the index of the coefficient.
   */
  *coefficientsWithExponents() {
    for (const [index, coefficient] of this.coefficients.entries()) {
      const exponent = this.coefficients.length - 1 - index;
      yield [coefficient, exponent, index];
    }
  }
}

<?php

// Map the preferred restaurants and activities to the people's names.
$people = array(
    "Max" => array(
        "restaurants" => ["Italienisch", "Griechisch", "Chinesisch"],
        "activities" => ["Tanzen", "Kinobesuch", "Museum"]
    ),
    "Hannah" => array(
        "restaurants" => ["Chinesisch", "Italienisch", "Griechisch"],
        "activities" => ["Tanzen", "Museum"]
    ),
    "Petra" => array(
        "restaurants" => ["Italienisch", "Steakhouse", "Chinesisch"],
        "activities" => ["Karaoke", "Museum", "Tanzen"]
    ),
    "Günther" => array(
        "restaurants" => ["Italienisch", "Steakhouse", "Chinesisch"],
        "activities" => ["Tanzen", "Museum", "Kinobesuch"]
    )
);

// Match all restaurants and activities to the number of their occurrence.
$restaurantCounts = [];
$activityCounts = [];
foreach ($people as $preferences) {
    foreach ($preferences["restaurants"] as $restaurant) {
        $restaurantCounts[$restaurant] = isset($restaurantCounts[$restaurant]) ? $restaurantCounts[$restaurant] + 1 : 1;
    }
    foreach ($preferences["activities"] as $activity) {
        $activityCounts[$activity] = isset($activityCounts[$activity]) ? $activityCounts[$activity] + 1 : 1;
    }
}

// Find the restaurant and activity with the highest amount.
$preferredRestaurant = null;
$preferredActivity = null;
foreach ($restaurantCounts as $restaurant => $count) {
    if ($preferredRestaurant === null || $count > $restaurantCounts[$preferredRestaurant]) {
        $preferredRestaurant = $restaurant;
    }
}
foreach ($activityCounts as $activity => $count) {
    if ($preferredActivity === null || $count > $activityCounts[$preferredActivity]) {
        $preferredActivity = $activity;
    }
}

// Print the results.
echo "The algorithm found the following preferences.</br>";
echo $preferredRestaurant !== null ? "Preferred restaurant: <b>$preferredRestaurant</b></br>" : "No restaurant preferences were specified.</br>";
echo $preferredActivity !== null ? "Preferred activity: <b>$preferredActivity</b></br>" : "No activity preferences were specified.</br>";

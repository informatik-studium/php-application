<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <title>Newton</title>

  <script src="polynomial-function.js"></script>
  <script src="newton-method.js"></script>
  <script src="newton-calculation.js"></script>
</head>

<body>
  <form action="newton.php" method="post">
    <?php
      // Keep track of the amount of fields to be generated using a hidden field.
      // The value is initialized to 3 and increased by one on each form submission.
      $_POST["fieldAmount"] = !isset($_POST["fieldAmount"]) ? 3 : $_POST["fieldAmount"] + 1;
    ?>
    <input type="hidden" name="fieldAmount" value="<?php echo $_POST['fieldAmount']; ?>" />

    <div>
      <button type="submit" id="add-field">Add field</button>

      <?php
        // Generate the fields for the polynomial function.
        // Its values are kept after each form submission.
        for ($i = $_POST["fieldAmount"] - 1; $i >= 0; $i--) {
          $value = isset($_POST["coefficient[$i]"]) ? $_POST["coefficient[$i]"] : "0";
          echo "<input type='number' id='coefficient[$i]' name='coefficient[$i]' value='$value' style='width: 2.5rem;' />";

          $label = $i > 1 ? "x<sup>$i</sup> + " : ($i === 1 ? "x + " : "");
          echo "<label for='coefficient[$i]' style='margin-left: 0.5rem;'>$label</label>";
        }
      ?>
    </div>

    <div style="margin-top: 1rem;">
      <?php
        // Keep track of the lower and upper bounds of the interval.
        // These values are kept after each form submission.
        $lowerBound = isset($_POST["lowerBound"]) ? $_POST["lowerBound"] : "-10";
        echo "<label for='lower-bound'>Lower bound:</label>";
        echo "<input type='number' id='lower-bound' name='lowerBound' value='$lowerBound' style='width: 2.5rem; margin-left: 0.25rem;' />";

        $upperBound = isset($_POST["upperBound"]) ? $_POST["upperBound"] : "10";
        echo "<label for='upper-bound' style='margin-left: 0.5rem;'>Upper bound:</label>";
        echo "<input type='number' id='upper-bound' name='upperBound' value='$upperBound' style='width: 2.5rem; margin-left: 0.25rem;' />";
      ?>
    </div>
  </form>

  <button onclick="startCalculation()" id="calculate" style="margin-top: 1rem;">Calculate roots</button>

  <div id="function-wrapper" style="display: none; margin-top: 1rem;">
    <label for="function">Function:</label>
    <span id="function"></span>
  </div>

  <div id="result-wrapper" style="display: none; margin-top: 1rem;">
    <label for="result">Result:</label>
    <span id="result"></span>
  </div>
</body>

</html>
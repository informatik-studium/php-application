/**
 * Helper class for Newton's method.
 *
 * @see https://en.wikipedia.org/wiki/Newton%27s_method
 */
class NewtonMethod {
  /**
   * Find the roots of a polynomial function using Newton's method.
   *
   * @static
   * @param {PolynomialFunction} f The polynomial function to find the roots of.
   * @param {number} lowerBound The lower bound of the interval to search for the roots.
   * @param {number} upperBound The upper bound of the interval to search for the roots.
   * @param {number} guessAmount The amount of initial guesses to use.
   * @param {number} precision The precision of the roots.
   * @returns {Promise<number[]>} The roots of the polynomial function in the interval.
   * @example findRoots(new PolynomialFunction([4, -4, -2, 1]), -1, 1, 100, 3) // [-0.585, 0.344]
   */
  static async findRoots(f, lowerBound, upperBound, guessAmount, precision) {
    const step = (upperBound - lowerBound) / (guessAmount - 1);
    const calculations = [];
    for (const x0 of NewtonMethod.range(lowerBound, upperBound, step)) {
      calculations.push(
        new Promise((resolve) => {
          const worker = new Worker("newton-worker.js");
          worker.addEventListener("message", (e) =>
            resolve(e.data.success ? e.data.x1 : null)
          );
          worker.postMessage({ f, x0, lowerBound, upperBound, precision });
        })
      );
    }
    const roots = await Promise.all(calculations);
    return [...new Set(roots.filter(Boolean))];
  }

  /**
   * Generate a range of numbers from a lower bound to an upper bound with a step.
   *
   * @generator
   * @param {number} lowerBound The lower bound of the range.
   * @param {number} upperBound The upper bound of the range.
   * @param {number} step The step of the range.
   * @yields {number} The next number in the range.
   */
  static *range(lowerBound, upperBound, step) {
    for (let i = lowerBound; i <= upperBound; i += step) {
      yield i;
    }
  }
}

/**
 * Start the calculation of the roots using Newton's method.
 *
 * @returns {Promise<void>}
 */
async function startCalculation() {
  // Find the HTML elements in the document.
  const addFieldButton = document.getElementById("add-field");
  const coefficientInputs = Array.from(
    document.querySelectorAll("input[name^='coefficient']")
  );
  const lowerBoundInput = document.getElementById("lower-bound");
  const upperBoundInput = document.getElementById("upper-bound");
  const calculateButton = document.getElementById("calculate");
  const functionWrapper = document.getElementById("function-wrapper");
  const functionText = document.getElementById("function");
  const resultWrapper = document.getElementById("result-wrapper");
  const resultText = document.getElementById("result");

  // Show an error message if there was no coefficient other than zero provided.
  const coefficients = coefficientInputs.map((input) => Number(input.value));
  if (
    coefficients.length === 0 ||
    coefficients.every((coefficient) => coefficient === 0)
  ) {
    alert("Please enter at least one non-zero coefficient.");
    return;
  }

  // Disable the inputs during the calculation.
  addFieldButton.disabled = true;
  coefficientInputs.forEach((input) => (input.disabled = true));
  calculateButton.disabled = true;
  functionWrapper.style.display = "none";
  resultWrapper.style.display = "none";

  const f = new PolynomialFunction(coefficients);
  const lowerBound = Number(lowerBoundInput.value);
  const upperBound = Number(upperBoundInput.value);
  const guessAmount = 100;
  const precision = 6;

  const roots = await NewtonMethod.findRoots(
    f,
    lowerBound,
    upperBound,
    guessAmount,
    precision
  );

  // Print the result text.
  functionText.innerText = f.toString();
  resultText.innerText =
    roots.length > 0 ? roots.join(", ") : "No roots found in the interval.";

  // Enable the inputs again.
  addFieldButton.disabled = false;
  coefficientInputs.forEach((input) => (input.disabled = false));
  calculateButton.disabled = false;
  functionWrapper.style.display = "block";
  resultWrapper.style.display = "block";
}
